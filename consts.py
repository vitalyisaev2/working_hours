class Header:
    ACTIVITY = 'Activity'
    DATE = 'Date'
    DURATION = 'Duration'
    END_TIME = 'EndTime'
    WEEK_NUMBER = 'WeekNumber'
    WORKING_DAY = 'WorkingDay'
    CUMULATIVE = 'Cumulative'


class Activity:
    START = 'Начало рабочего дня'
    VACATION = 'Отпуск'
    HOLIDAY = 'Праздник'
    BREAK = 'Перерыв'
