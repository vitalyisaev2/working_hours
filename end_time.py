import datetime
import pandas as pd
from consts import Header, Activity
from abc import ABC, abstractmethod


class State(ABC):
    @abstractmethod
    def eval(self, curr: pd.Series):
        pass

    @staticmethod
    def next_day(date: pd.Timestamp):
        return date + datetime.timedelta(days=1)

    @staticmethod
    def combine(date: pd.Timestamp, end_time: pd.Timestamp) -> pd.Timestamp:
        # print(type(date.day), type(date.month), type(date.day), type(end_time.hour), type(end_time.minute))
        value = datetime.datetime(year=date.year,
                                  month=date.month,
                                  day=date.day,
                                  hour=end_time.hour,
                                  minute=end_time.minute)
        return pd.to_datetime(value)


class Initial(State):
    def eval(self, curr: pd.Series) -> (pd.Timestamp, State):
        if pd.isna(curr[Header.END_TIME]):
            return pd.NaT, self
        else:
            return self.combine(curr[Header.DATE], curr[Header.END_TIME]), Ordinary(curr)


class Ordinary(State):
    def __init__(self, prev: pd.Series):
        self.prev = prev

    def eval(self, curr: pd.Series) -> (pd.Timestamp, State):
        curr_date, curr_end_time = curr[Header.DATE], curr[Header.END_TIME]
        prev_date, prev_end_time = self.prev[Header.DATE], self.prev[Header.END_TIME]
        if pd.isna(curr_end_time):
            return pd.NaT, Initial()
        elif curr_end_time > prev_end_time:
            if curr_date == prev_date:
                # рабочий день продолжается
                return self.combine(curr_date, curr_end_time), Ordinary(curr)
        elif curr_end_time < prev_end_time:
            if curr_date == prev_date:
                # начался следующий день, но работа ещё идёт (ушли в ночь)
                next_day = self.next_day(curr_date)
                return self.combine(next_day, curr_end_time), Overnight(curr)
            elif curr_date > prev_date:
                # утро следующего дня
                if curr[Header.ACTIVITY] != Activity.START:
                    raise ValueError("Invalid activity", self.prev, curr)
                return pd.NaT, Initial()
            else:
                raise ValueError(self.prev, curr)
        else:
            raise ValueError(self.prev, curr)


class Overnight(State):
    def __init__(self, prev: pd.Series):
        self.prev = prev

    def eval(self, curr: pd.Series) -> (pd.Timestamp, State):
        curr_date = curr[Header.DATE]
        prev_date = self.prev[Header.DATE]
        curr_end_time = curr[Header.END_TIME]
        if curr_date > prev_date:
            # начался новый день
            return pd.NaT, Initial()
        elif curr_date == prev_date:
            # продолжаем овертаймить
            next_day = self.next_day(curr_date)
            return self.combine(next_day, curr_end_time), self
        else:
            raise ValueError(self.prev, curr)


def derive(df: pd.DataFrame) -> pd.Series:
    # парсим время как есть
    df[Header.END_TIME] = pd.to_datetime(df[Header.END_TIME], format='%H:%M')

    # затем присовокупляем к нему дату
    result = []
    state = Initial()

    for index, row in df.iterrows():
        try:
            value, state = state.eval(row)
            result.append(value)
        except Exception as e:
            raise ValueError(f"{e}: {index}\n{row}")

    return pd.Series(result)
