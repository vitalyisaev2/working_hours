#!/usr/bin/env python3
import pandas as pd
import datetime

from consts import Header, Activity
import end_time

pd.set_option('display.max_rows', 40)
pd.set_option('display.max_columns', None)
pd.set_option('display.width', None)
pd.set_option('display.max_colwidth', -1)


def load_data() -> pd.DataFrame:
    df = pd.read_csv("data.csv")
    df[Header.DATE] = pd.to_datetime(df[Header.DATE], format='%d.%m.%Y')
    df[Header.END_TIME] = end_time.derive(df)
    df[Header.DURATION] = df[Header.END_TIME].diff()
    df[Header.WORKING_DAY] = df.apply(lambda x: is_working_day(x), axis=1)
    return df


def validate_data(src: pd.DataFrame):
    df = src[src[Header.ACTIVITY] == Activity.START]
    df = df[~df[Header.DURATION].isnull()]
    if len(df) > 0:
        raise ValueError("start of the day has duration: {}".format(df))


def is_working_day(row: pd.Series) -> bool:
    return row[Header.ACTIVITY] not in (Activity.VACATION, Activity.HOLIDAY) and \
           row[Header.DATE].weekday() not in (5, 6)


def count_working_days(src: pd.DataFrame) -> int:
    df = src[src[Header.WORKING_DAY]]
    return len(df[Header.DATE].unique())


def count_non_working_days(src: pd.DataFrame) -> int:
    df = src[~src[Header.WORKING_DAY]]
    return len(df[Header.DATE].unique())


def count_working_hours_expected(working_days: int) -> datetime.timedelta:
    return datetime.timedelta(hours=working_days * 8)


def count_working_hours_actual(src: pd.DataFrame) -> datetime.timedelta:
    # любая активность, кроме перерыва, считается рабочей
    df = src[src[Header.ACTIVITY] != Activity.BREAK]
    return df[Header.DURATION].sum()


def day_duration_with_breaks(src: pd.DataFrame) -> datetime.timedelta:
    return src[src[Header.WORKING_DAY]].groupby(Header.DATE)[Header.DURATION].sum().where(
        lambda x: x > datetime.timedelta(0)).dropna().mean()


def day_duration_without_breaks(src: pd.DataFrame) -> datetime.timedelta:
    df = src[src[Header.WORKING_DAY]]
    df = df[df[Header.ACTIVITY] != Activity.BREAK]

    return df.groupby(Header.DATE)[Header.DURATION].sum().where(lambda x: x > datetime.timedelta(0)).dropna().mean()


def context_switches(src: pd.DataFrame) -> float:
    return src[src[Header.WORKING_DAY]].groupby(Header.DATE)[Header.END_TIME].size().mean()


def working_time_structure(src: pd.DataFrame) -> pd.DataFrame:
    df = src[~src[Header.DURATION].isnull()]
    df = df[df[Header.ACTIVITY] != Activity.BREAK]
    df = df.groupby(Header.ACTIVITY). \
        agg({Header.DURATION: 'sum'}). \
        apply(lambda x: x / x.sum()). \
        sort_values(by=Header.DURATION, ascending=False)
    df[Header.DURATION] = df[Header.DURATION].apply(lambda x: "{0:.2f}%".format(x * 100))
    return df


def main():
    df = load_data()
    validate_data(df)

    working_days = count_working_days(df)
    non_working_days = count_non_working_days(df)
    working_hours_expected = count_working_hours_expected(working_days)
    working_hours_actual = count_working_hours_actual(df)

    print(f"Рабочие дни: {working_days}")
    print(f"Нерабочие дни: {non_working_days}")
    print(f"Рабочее время (ожидаемое значение): {working_hours_expected}")
    print(f"Рабочее время (фактическое значение): {working_hours_actual}")
    print(f"Рабочее время (фактическое / ожидаемое): {working_hours_actual / working_hours_expected:.0%}")
    print(f"Средняя длительность рабочего дня (пн-пт, с перерывами): {day_duration_with_breaks(df)}")
    print(f"Средняя длительность рабочего дня (пн-пт, без перерывов): {day_duration_without_breaks(df)}")
    print(f"Среднее количество переключений контекста в день (пн-пт): {context_switches(df):.2f}")
    print("\nСтруктура рабочего времени:")
    print(working_time_structure(df))

    df.to_csv("out.csv")


if __name__ == "__main__":
    main()
